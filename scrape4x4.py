#!/usr/bin/python3

import requests
from bs4 import BeautifulSoup
import re
import matplotlib.pyplot as pyplot
import random
import hashlib

URL = 'https://petting.town/stats'
page = requests.get(URL)

soup = BeautifulSoup(page.content, 'html.parser')

tab = soup.find(id='per-alltime').find('table')
loves = tab.findAll(class_='stats-table-text')
names = tab.findAll('span', class_='stats-pet-text')
tabg = tab.findNext('table')
lovesg = tabg.findAll(class_='stats-table-text')
namesg = tabg.findAll('span', class_='stats-pet-text')

pat = re.compile('.*scope.*');
love = [int(lov.text.strip()) for lov in loves if not pat.match(str(lov))]
loveg = [int(lov.text.strip()) for lov in lovesg if not pat.match(str(lov))]

name = [nam.text.strip() for nam in names]
nameg = [nam.text.strip() for nam in namesg]
name = [nam[:10] if len(nam) > 10 else nam for nam in name]
nameg = [nam[:10] if len(nam) > 10 else nam for nam in nameg]
love = love[:len(name)]
loveg = loveg[:len(nameg)]

# print(len(love))
# print(len(name))
# print(len(loveg))
# print(len(nameg))


def bar(axis, name, love):
    bars = axis.bar(name, love)
    for n, b in zip(name, bars):
        random.seed(int(hashlib.sha1(n.encode('utf-8')).hexdigest(), 16)+19)
        rgb = [0, random.randint(0,255), random.randint(200,255)]
        random.shuffle(rgb)
        color = "#%02x%02x%02x" % tuple(rgb)
        b.set_color(color)

fig, ((ax1, ax2),(ax3, ax4)) = pyplot.subplots(2, 2, figsize=(28,19));
pyplot.setp(ax1.xaxis.get_majorticklabels(), rotation=90)
pyplot.setp(ax2.xaxis.get_majorticklabels(), rotation=90)
pyplot.setp(ax3.xaxis.get_majorticklabels(), rotation=90)
pyplot.setp(ax4.xaxis.get_majorticklabels(), rotation=90)

fig.suptitle('PETTING TOWN STATISTICS (from https://petting.town/stats, updates every 10 min, see aagrinder.xyz/files/lovelog.png for logarithmic)', fontsize=20)
ax1.set_title('all-time love RECEIVED', fontsize=30)
ax1.set_ylabel('all-time love RECEIVED (in unit of 1 love)', fontsize=20)
ax2.set_title('all-time love GIVEN', fontsize=30)
ax2.set_ylabel('all-time love GIVEN (in unit of 1 love)', fontsize=20)
ax3.set_title('per-year love RECEIVED', fontsize=30)
ax3.set_ylabel('per-year love RECEIVED (in unit of 1 love)', fontsize=20)
ax4.set_title('per-year love GIVEN', fontsize=30)
ax4.set_ylabel('per-year love GIVEN (in unit of 1 love)', fontsize=20)

bar(ax1, name, love)
bar(ax2, nameg, loveg)
bar(ax3, name, love)
bar(ax4, nameg, loveg)
# pyplot.show()
pyplot.savefig('love.png', dpi=300, bbox_inches='tight')





fig, ((ax1, ax2),(ax3, ax4)) = pyplot.subplots(2, 2, figsize=(28,19));
pyplot.setp(ax1.xaxis.get_majorticklabels(), rotation=90)
pyplot.setp(ax2.xaxis.get_majorticklabels(), rotation=90)
pyplot.setp(ax3.xaxis.get_majorticklabels(), rotation=90)
pyplot.setp(ax4.xaxis.get_majorticklabels(), rotation=90)

fig.suptitle('PETTING TOWN STATISTICS (from https://petting.town/stats, updates every 10 min, see aagrinder.xyz/files/love.png for normal version)', fontsize=20)
ax1.set_title('all-time love RECEIVED (logarithmic)', fontsize=30)
ax1.set_ylabel('all-time love RECEIVED (logarithmic)', fontsize=20)
ax2.set_title('all-time love GIVEN (logarithmic)', fontsize=30)
ax2.set_ylabel('all-time love GIVEN (logarithmic)', fontsize=20)
ax3.set_title('per-year love RECEIVED (logarithmic)', fontsize=30)
ax3.set_ylabel('per-year love RECEIVED (logarithmic)', fontsize=20)
ax4.set_title('per-year love GIVEN (logarithmic)', fontsize=30)
ax4.set_ylabel('per-year love GIVEN (logarithmic)', fontsize=20)
ax1.set_yscale('log')
ax2.set_yscale('log')
ax3.set_yscale('log')
ax4.set_yscale('log')

bar(ax1, name, love)
bar(ax2, nameg, loveg)
bar(ax3, name, love)
bar(ax4, nameg, loveg)
# pyplot.show()
pyplot.savefig('lovelog.png', dpi=300, bbox_inches='tight')
