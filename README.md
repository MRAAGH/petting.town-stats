# [petting.town](https://petting.town) unofficial stats

# >>> [mazie.rocks/petting/](https://mazie.rocks/petting/) <<<

![logarithmic graph](https://mazie.rocks/petting/all-rec-log.png)

Scrapes data from https://petting.town/stats and makes some graphs and puts them on a little [webpage](https://mazie.rocks/petting/)!

# Install

```
git clone https://gitlab.com/MRAAGH/petting.town-stats.git
cd petting.town-stats
pip install -r requirements.txt
```

# Run

`python3 ./scrape.py`
