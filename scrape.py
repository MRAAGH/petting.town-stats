#!/usr/bin/python3

import requests
from bs4 import BeautifulSoup
import re
import numpy
import matplotlib.pyplot as pyplot
import random
import hashlib
import datetime

URL = 'https://petting.town/stats'
page = requests.get(URL)

date = datetime.datetime.now()
year = date.year
month = date.month
THISMONTH = str(year)+'_'+str(month)
PREVMONTH = str(year if month > 1 else year-1)+'_'+str((month+10)%12+1)

img_width = int(4736/6)
img_height = int(2327/6)

soup = BeautifulSoup(page.content, 'html.parser')
tab = soup.find(id='per-alltime').find('table')
mtab = soup.find(id='per-month').find('table')


def sanitize(s):
    return s.replace('<','').replace('>','').replace('"','').strip()

def urlencode(s):
    return s.replace(' ','-')

def processSection(tab, htmlfilename, titlee, fileprefix, titleformat, ISMONTHLY):
    loves = tab.findAll(class_='stats-table-text')
    names = tab.findAll('span', class_='stats-pet-text')
    tabg = tab.findNext('table')
    lovesg = tabg.findAll(class_='stats-table-text')
    namesg = tabg.findAll('span', class_='stats-pet-text')

    pat = re.compile('.*scope.*');
    love = [int(lov.text.strip()) for lov in loves if not pat.match(str(lov))]
    loveg = [int(lov.text.strip()) for lov in lovesg if not pat.match(str(lov))]

    long_name = [sanitize(nam.text) for nam in names]
    long_nameg = [sanitize(nam.text) for nam in namesg]
    name = [nam[:10] if len(nam) > 10 else nam for nam in long_name]
    nameg = [nam[:10] if len(nam) > 10 else nam for nam in long_nameg]
    love = love[:len(name)]
    loveg = loveg[:len(nameg)]

    love_rec = numpy.array(love)
    love_giv = numpy.array(loveg)
    total_love_rec = love_rec.sum()
    total_love_giv = love_giv.sum()
    max_love_rec = numpy.max(love_rec)
    max_love_giv = numpy.max(love_giv)
    cum_love_rec = numpy.cumsum(love_rec)
    cum_love_giv = numpy.cumsum(love_giv)

    max_percent_rec = round(max_love_rec/total_love_rec*1000)/10
    max_rec_name = long_name[numpy.argmax(love_rec)]

    below_1_percent = numpy.where(love_rec < 0.001 * total_love_rec)[0]
    names_below_1_percent = [long_name[i] for i in below_1_percent]
    numpy.random.shuffle(names_below_1_percent)
    shoutouts_html = "\n".join(['<li><a href="http://petting.town/{1}">{0}</a></li>'.format(lname, urlencode(lname)) for lname in names_below_1_percent])
    # whatpercent_below_1_percent = round(len(below_1_percent)/len(love_rec)*1000)/10

    anonymous_love = total_love_rec - total_love_giv
    anonymous_love_percent = round(anonymous_love/total_love_rec*1000)/10

    top_20_love_rec = cum_love_rec[int(len(cum_love_rec)*0.2)]
    top_20_love_rec_percent = round(top_20_love_rec/total_love_rec*1000)/10


    TEMPLATE = """<!DOCTYPE html>
    <html lang="en">
    <head>
    <title>petting.town stats (UNOFFICIAL)</title>
    <meta charset="utf-8">
    <style>
    body {{
        background-color: #1e2330;
        color: white;
        font-size: 1.5em;
    }}
    hr {{
        border-top: 5px solid #8d97c2;
    }}
    a:link {{
        color: #cf8ec4;
    }}
    a:visited {{
        color: #8d97c2;
    }}
    h1,h2,p,ul,li {{
        text-align: center;
    }}
    ul {{
        list-style-type: none;
        padding: 0px;
    }}
    </style>
    </head>
    <body>
    <h1>PETTING.TOWN UNOFFICIAL STATS ({titlee})</h1>
    <p>These stats are collected from <a href="https://petting.town/stats">petting.town</a>
    and are automatically updated every 10 minutes! Enjoy!</p>
    <p>
    {alltime_button}
    {month_button}
    </p>
    <hr />
    <h2>Graphs!</h2>
    <p>
    <a href="{fileprefix}-rec{filesuffix}.png"><img alt="{fileprefix}-rec{filesuffix}.png" width="{img_width}" height="{img_height}" src="{fileprefix}-rec{filesuffix}.png"></a>
    <a href="{fileprefix}-giv{filesuffix}.png"><img alt="{fileprefix}-giv{filesuffix}.png" width="{img_width}" height="{img_height}" src="{fileprefix}-giv{filesuffix}.png"></a>
    </p>
    <p>
    {log_toggle_button}
    </p>
    <hr />
    <h2>Discussion</h2>
    <p>{titlee} LOVE RECEIVED: {total_love_rec} ({max_percent_rec}% by {max_rec_name})</p>
    <p>{titlee} LOVE GIVEN: {total_love_giv} (remaining {anonymous_love_percent}%
    of love was given by anonymous users)</p>
    {shoutouts_full_html}
    <p>
    Code for this page at <a href="https://gitlab.com/MRAAGH/petting.town-stats">GitLab</a>
    </p>
    </body>
    </html>
    """

    SHOUT_TEMPLATE = """
    <p>Top 20% users hold {top_20_love_rec_percent}% of the love.
    Show some attention to the ones with less than 0.1% love! Shoutouts to:
    <ul>
    {shoutouts_html}
    </ul>
    </p>
    """

    for ISLOG in [True, False]:
        log = '_log' if ISLOG else ''
        log_toggle_button = '<a href="{0}{1}.html">{2}</a>'.format(
                THISMONTH if ISMONTHLY else 'index',
                '' if ISLOG else '_log',
                'switch to normal graph' if ISLOG else 'switch to log graph'
                )

        out_html = TEMPLATE.format(
                alltime_button='<a href="index'+log+'.html">switch to alltime</a>' if ISMONTHLY
                    else '',
                month_button='<a href="'+PREVMONTH+log+'.html">go one month back</a>' if ISMONTHLY
                    else '<a href="'+THISMONTH+log+'.html">switch to monthly</a>',
                img_width=img_width,
                img_height=img_height,
                total_love_rec=total_love_rec,
                max_percent_rec=max_percent_rec,
                max_rec_name=max_rec_name,
                total_love_giv=total_love_giv,
                anonymous_love_percent=anonymous_love_percent,
                shoutouts_full_html=SHOUT_TEMPLATE.format(
                    top_20_love_rec_percent=top_20_love_rec_percent,
                    shoutouts_html=shoutouts_html) if not ISMONTHLY else '',
                titlee=titlee,
                fileprefix=fileprefix,
                filesuffix='-log' if ISLOG else '',
                log_toggle_button=log_toggle_button,
                )

        htmlsuffix = '_log.html' if ISLOG else '.html'

        with open(htmlfilename + htmlsuffix, 'w') as w:
            w.write(out_html)

    def bar(axis, name, love):
        bars = axis.bar(name, love)
        for n, b in zip(name, bars):
            random.seed(int(hashlib.sha1(n.encode('utf-8')).hexdigest(), 16)+19)
            rgb = [0, random.randint(0,255), random.randint(200,255)]
            random.shuffle(rgb)
            color = "#%02x%02x%02x" % tuple(rgb)
            b.set_color(color)


    fig = pyplot.figure(figsize=(19,8));
    pyplot.title(titleformat.format('RECEIVED'), fontsize=30)
    pyplot.ylabel(titleformat.format('RECEIVED')+' (in unit of 1 love)', fontsize=20)
    pyplot.xticks(rotation=90)
    bar(pyplot, name, love)
    pyplot.savefig(fileprefix+'-rec.png', dpi=300, bbox_inches='tight')

    fig = pyplot.figure(figsize=(19,8));
    pyplot.title(titleformat.format('GIVEN'), fontsize=30)
    pyplot.ylabel(titleformat.format('GIVEN')+' (in unit of 1 love)', fontsize=20)
    pyplot.xticks(rotation=90)
    bar(pyplot, nameg, loveg)
    pyplot.savefig(fileprefix+'-giv.png', dpi=300, bbox_inches='tight')

    fig = pyplot.figure(figsize=(19,8));
    pyplot.title(titleformat.format('RECEIVED')+' (logarithmic)', fontsize=30)
    pyplot.ylabel(titleformat.format('RECEIVED')+' (logarithmic)', fontsize=20)
    pyplot.yscale('log')
    pyplot.xticks(rotation=90)
    bar(pyplot, name, love)
    pyplot.savefig(fileprefix+'-rec-log.png', dpi=300, bbox_inches='tight')

    fig = pyplot.figure(figsize=(19,8));
    pyplot.title(titleformat.format('GIVEN')+' (logarithmic)', fontsize=30)
    pyplot.ylabel(titleformat.format('GIVEN')+' (logarithmic)', fontsize=20)
    pyplot.yscale('log')
    pyplot.xticks(rotation=90)
    bar(pyplot, nameg, loveg)
    pyplot.savefig(fileprefix+'-giv-log.png', dpi=300, bbox_inches='tight')

processSection(
        tab,
        'index',
        'ALLTIME',
        'all',
        'all-time love {0}',
        False,
        )

processSection(
        mtab,
        THISMONTH,
        THISMONTH,
        THISMONTH,
        'love {0} in month '+THISMONTH,
        True,
        )

