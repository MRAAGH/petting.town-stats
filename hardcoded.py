#!/usr/bin/python3

# This is where the original graph came from,
# It's hardcoded data, copied manually from the stats page.
# Scraping is better!

import numpy
import matplotlib.pyplot as pyplot

lis = [

(11200, 'Sorunome'),
(2358, 'Maze'),
(675, 'SillyHors'),
(245, 'Onnellia~'),
(225, 'Dark'),
(136, 'Sugar'),
(134, 'Nazz'),
(118, 'Drieuq'),
(93, 'Docckylle'),
(78, 'Pixel'),
(75, 'Cyan'),
(73, 'Violet Ray'),
(55, 'Asmodias'),
(51, 'Autumn Dawn'),
(50, 'Hazel'),
(48, 'Arcane Blackwood'),
(48, 'The letter P'),
(23, 'MTRNord'),
(23, 'lou_de_sel'),
(19, 'Moonbow'),
(17, 'Aether Wake'),
(17, 'Cyrius Solstice'),
(15, 'Zake2002'),

]

love = [a[0] for a in lis]
name = [a[1] for a in lis]

a=(numpy.array(love).sum())
b=(numpy.array(love[:2]).sum())

alpha=0.8

print(a)
print(a*alpha)
print(b)
print(a*alpha-b)

pyplot.xticks(rotation=90)

pyplot.plot(love)
pyplot.show()
