#!/usr/bin/python3

import numpy
import matplotlib.pyplot as pyplot
import random
import hashlib


bars = pyplot.bar(numpy.arange(100), numpy.arange(100))
for i in range(100):
    random.seed(int(hashlib.sha1('Sorunome'.encode('utf-8')).hexdigest(), 16)+i)
    rgb = [0, random.randint(0,255), random.randint(200,255)]
    random.shuffle(rgb)
    color = "#%02x%02x%02x" % tuple(rgb)
    bars[i].set_color(color)

pyplot.show()
